<?php

use Illuminate\Database\Seeder;

class RoleUserPivotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //creating admin user
        $user = factory(App\User::class)->create([
            'email' => 'admin@test.com',
        ]);
        //creating and linking admin role
        $role = factory(App\Role::class)->create([
            'name' => __('roles.admin')
        ]);
        \DB::table('role_user')->insert([
            'role_id' => $role->id,
            'user_id' => $user->id,
        ]);
        //creating student user
        $user = factory(App\User::class)->create([
            'email' => 'student@test.com',
        ]);
        //creating and linking student role
        $role = factory(App\Role::class)->create([
            'name' => __('roles.student')
        ]);
        \DB::table('role_user')->insert([
            'role_id' => $role->id,
            'user_id' => $user->id,
        ]);
        //creating teacher user
        $user = factory(App\User::class)->create([
            'email' => 'teacher@test.com',
        ]);
        //creating and linking teacher role
        $role = factory(App\Role::class)->create([
            'name' => __('roles.teacher')
        ]);
        \DB::table('role_user')->insert([
            'role_id' => $role->id,
            'user_id' => $user->id,
        ]);
        //creating guardian user
        $user = factory(App\User::class)->create([
            'email' => 'guardian@test.com',
        ]);
        //creating and linking guardian role
        $role = factory(App\Role::class)->create([
            'name' => __('roles.guardian')
        ]);
        \DB::table('role_user')->insert([
            'role_id' => $role->id,
            'user_id' => $user->id,
        ]);
    }
}

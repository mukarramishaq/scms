<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(App\User::class)->create([
            'email' => 'admin@test.com',
        ]);
        factory(App\User::class)->create([
            'email' => 'student@test.com',
        ]);
        factory(App\User::class)->create([
            'email' => 'teacher@test.com',
        ]);
        factory(App\User::class)->create([
            'email' => 'guardian@test.com',
        ]);

        factory(App\User::class, 10)->create();
    }
}

<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        foreach(__('roles') as $key => $value){
            factory(App\Role::class)->create([
                'name' => $value
            ]); 
        }
    }
}

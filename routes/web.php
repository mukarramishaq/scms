<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome')->middleware('guest');
Route::get('/home', function(){
    $homeRoutes = [
        'student' => 'student.home',
        'teacher' => 'teacher.home',
        'guardian' => 'guardian.home',
        'admin' => 'admin.home',
    ];
    return redirect()->route($homeRoutes[strtolower(Auth::user()->roles()->first()->name)]);
})->name('home')->middleware('auth');

Auth::Routes();

Route::get('/developer/contact', ['as'=>'developer.contact', 'uses'=>'DeveloperController@contact']);

/**
 * Student Related Routes
 */
Route::group(['prefix'=>'student', 'namespace'=>'Student'], function(){
    /**
     * Not Accessible by logged in user
     */
    Route::group(['middleware'=>'guest'], function(){
        //Register
        Route::get('register', ['as'=>'student.auth.register.get', 'uses'=>'AuthController@registerGET']);
        Route::post('register', ['as'=>'student.auth.register.post', 'uses'=>'AuthController@registerPOST']);
        //login
        Route::get('login', ['as'=>'student.auth.login.get', 'uses'=>'AuthController@loginGET']);
        Route::post('login', ['as'=>'student.auth.login.post', 'uses'=>'AuthController@loginPOST']);
        
    });

     /**
      * Strictly accessible by logged in user
      */
    Route::group(['middleware'=>'auth'], function(){
        //logout
        Route::get('logout', ['as' => 'student.auth.logout', 'uses'=>'AuthController@logout']);
        //home route
        Route::get('home', ['as'=>'student.home', 'uses'=>'HomeController@index']);
    });
});

/**
 * Teacher Related Routes
 */
Route::group(['prefix'=>'teacher', 'namespace'=>'Teacher'], function(){
    /**
     * Not Accessible by logged in user
     */
    Route::group(['middleware'=>'guest'], function(){
        //Register
        Route::get('register', ['as'=>'teacher.auth.register.get', 'uses'=>'AuthController@registerGET']);
        Route::post('register', ['as'=>'teacher.auth.register.post', 'uses'=>'AuthController@registerPOST']);
        //login
        Route::get('login', ['as'=>'teacher.auth.login.get', 'uses'=>'AuthController@loginGET']);
        Route::post('login', ['as'=>'teacher.auth.login.post', 'uses'=>'AuthController@loginPOST']);
    });

     /**
      * Strictly accessible by logged in user
      */
    Route::group(['middleware'=>'auth'], function(){
        //logout
        Route::get('logout', ['as' => 'teacher.auth.logout', 'uses'=>'AuthController@logout']);
        //home route
        Route::get('home', ['as'=>'teacher.home', 'uses'=>'HomeController@index']);
    });
});

/**
 * Guardian Related Routes
 */
Route::group(['prefix'=>'guardian', 'namespace'=>'Guardian'], function(){
    /**
     * Not Accessible by logged in user
     */
    Route::group(['middleware'=>'guest'], function(){
        //Register
        Route::get('register', ['as'=>'guardian.auth.register.get', 'uses'=>'AuthController@registerGET']);
        Route::post('register', ['as'=>'guardian.auth.register.post', 'uses'=>'AuthController@registerPOST']);
        //login
        Route::get('login', ['as'=>'guardian.auth.login.get', 'uses'=>'AuthController@loginGET']);
        Route::post('login', ['as'=>'guardian.auth.login.post', 'uses'=>'AuthController@loginPOST']);
    });

     /**
      * Strictly accessible by logged in user
      */
    Route::group(['middleware'=>'auth'], function(){
        //logout
        Route::get('logout', ['as' => 'guardian.auth.logout', 'uses'=>'AuthController@logout']);
        //home route
        Route::get('home', ['as'=>'guardian.home', 'uses'=>'HomeController@index']);
    });
});







/**
 * Admin Related Routes
 */
Route::group(['prefix'=>'admin', 'namespace'=>'Admin'], function(){
    /**
     * Not Accessible by logged in user
     */
    Route::group(['middleware'=>'guest'], function(){
        //Register
        Route::get('register', ['as'=>'admin.auth.register.get', 'uses'=>'AuthController@registerGET']);
        Route::post('register', ['as'=>'admin.auth.register.post', 'uses'=>'AuthController@registerPOST']);
        //login
        Route::get('login', ['as'=>'admin.auth.login.get', 'uses'=>'AuthController@loginGET']);
        Route::post('login', ['as'=>'admin.auth.login.post', 'uses'=>'AuthController@loginPOST']);
    });

     /**
      * Strictly accessible by logged in user
      */
    Route::group(['middleware'=>'auth'], function(){
        //logout
        Route::get('logout', ['as' => 'admin.auth.logout', 'uses'=>'AuthController@logout']);
        //home route
        Route::get('home', ['as'=>'admin.home', 'uses'=>'HomeController@index']);
    });
});
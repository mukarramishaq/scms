<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * REST API
 * All the endpoints relating to REST will be in rest group
 */
Route::group(['prefix'=>'rest'], function(){
    /**
     * Version 1
     * All routes relating to v1 will be in v1 group
     */
    Route::group(['prefix'=>'v1'], function(){
        /**
         * All authenticated routes will be in this group
         */
        Route::group(['middleware'=>'auth:api'], function(){
            Route::get('user', 'Api\AuthController@user');

            Route::get('/users', 'Api\UserController@index');
        });


        /**
         * All Non Authenticated routes will be here
         */

        /*-- Auth Routes */
        Route::post('login', 'Api\AuthController@login');
        Route::post('register', 'Api\AuthController@register');
        /* Auth Routes --*/
    });

    /**
     * Version 2
     * All routes relating to v2 will be in v2 group
     * Since, v2 will be released in future, so here is just skeleton and nothing else
     */
    Route::group(['prefix'=>'v2'], function(){
        /**
         * All authenticated routes will be in this group
         */
        Route::group(['middleware'=>'auth:api'], function(){

        });

        /**
         * All Non Authenticated routes will be here
         */
    });
});

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subject extends Model
{
    use SoftDeletes, Uuids;
    protected $dates = ['deleted_at'];
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    //
    /**
     * Retrieve all teachers assigned to a subject in all terms
     */
    public function teachers()
    {
        return $this->belongsToMany('App\Teacher')
            ->withPivot('section_id', 'term_id')
            ->withTimestaps();
    }

    /**
     * Retreive all students who have read or are reading this subjects
     */
    public function students()
    {
        return $this->belongsToMany('App\Student')
            ->withPivot('term_id')
            ->withTimestamps();
    }

}

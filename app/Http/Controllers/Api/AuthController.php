<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use Validator;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    //
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function register(Request $request)
    {
        //validate the form data
        $data = $request->data['attributes'];
        $validator = Validator::make($data, [
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|min:6'
        ]);
        //if validation fails
        if ($validator->fails()) {
            $response = [];
            $response['status'] = __('http.client_error.bad_request.status');
            $response['code'] = __('http.client_error.bad_request.code');
            $response['response_type'] = __('http.response_type.error');
            $response['message'] = 'Some fields are not valid';
            foreach($validator->errors()->all() as $errorMessage) {
                $error['detail'] = $errorMessage;
                $error['title'] = 'Field Validation Error';
                $response['errors'][] = $error;
            }
            return response()->json($response, (int)__('http.client_error.bad_request.code'));
        }
        //create a user
        $data = (object) $data;
        $user = new User([
            'name' => $data->name,
            'email' => $data->email,
            'password' => bcrypt($data->password)
        ]);
        //save the user to database
        $user->save();
        //return success data
        return response()->json([
            'status' => __('http.success.created.status'),
            'code' => __('http.success.created.code'),
            'response_type' => __('http.response_type.success'),
            'message' => 'Resource created successfully',
            'type'=>'user',
            'id' => $user->id,
            'attributes' => [
                'name' => $user->name,
                'email' => $user->email,
            ], 
        ], (int)__('http.success.created.code'));
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        //validate the form data
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        //if validation fails
        if ($validator->fails()) {
            $response = [];
            $response['status'] = __('http.client_error.bad_request.status');
            $response['code'] = __('http.client_error.bad_request.code');
            $response['response_type'] = __('http.response_type.error');
            $response['message'] = 'Some fields are not valid';
            foreach($validator->errors()->all() as $errorMessage) {
                $error['detail'] = $errorMessage;
                $error['title'] = 'Field Validation Error';
                $response['errors'][] = $error;
            }
            return response()->json($response, (int)__('http.client_error.bad_request.code'));
        }
        $credentials = $request->only(['email', 'password']);
        //if auth attempt fails
        if(!Auth::attempt($credentials)) {
            return response()->json([
                'status'=>__('http.client_error.unauthorized.status'),
                'code' => __('http.client_error.unauthorized.code'),
                'response_type' => __('http.response_type.error'),
                'errors' => array(
                    array(
                        'title' => 'Invalid Credentials',
                        'detail' => 'Username or Password is invalid'
                    )
                )
            ], (int)__('http.client_error.unauthorized.code'));
        }
        //otherwise get the authenticated user and create token
        $user = $request->user();
        $tokenResult = $user->createToken('The Educators Anabat Campus');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        //save token data to database
        $token->save();
        //return success data
        return response()->json([
            'status' => __('http.success.ok.status'),
            'code' => __('http.success.ok.code'),
            'response_type' => __('http.response_type.success'),
            'message' => 'Successfully logged in',
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'status' => __('http.client_error.bad_request.status'),
            'code' => __('http.client_error.bad_request.status'),
            'response_type' => __('http.response_type.success'),
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }
}

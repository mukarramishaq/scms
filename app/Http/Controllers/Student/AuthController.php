<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Rules\DoesStudentExist;
use Validator;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('welcome');
    }

    //
    /**
     * return register view for students
     */
    public function registerGET(Request $request)
    {
        //return view('student.auth.register');
        return redirect()->route('student.auth.login');
    }

    /**
     * register the student
     */
    public function registerPOST(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        $data = $request->only('name', 'email');
        $data['role_id'] = Role::where('name', '=', __('roles.student'))->first()->id;
        $data['password'] = bcrypt($request->password);
        //now create the user
        $user = User::create($data);
        return redirect()->back()->with('info', [
            'type' => 'success',
            'message' => "Registration successful. Please <a class='badge badge-primary' href='" . route('student.auth.login.get') . "'>Login</a> to continue!",
        ]);
    }

    /**
     * return login view for students
     */
    public function loginGET(Request $request)
    {
        return view('student.auth.login');
    }

    /**
     * login the student
     */
    public function loginPOST(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => [
                'required',
                'string',
                'email',
                new DoesStudentExist,
            ],
            'password' => 'required|string',
        ]);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        //attempt login
        if (Auth::attempt($request->only('email', 'password'))) {
            //verify its student
            $user = Auth::user();
            return redirect()->route('student.home');
        }
        return redirect()->back()->with('info', [
            'type' => 'error',
            'message' => 'Invalid Password',
        ]);
    }
}

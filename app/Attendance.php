<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attendance extends Model
{
    use SoftDeletes, Uuids;
    protected $dates = ['deleted_at'];
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    //
    /**
     * Retrieve related user
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Retrieve related term
     */
    public function term()
    {
        return $this->belongsTo('App\Term');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Teacher extends Model
{
    use SoftDeletes, Uuids;
    protected $dates = ['deleted_at'];
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    //
    /**
     * Retrieve related user
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function subjects()
    {
        return $this->belongsToMany('App\Subject')
            ->withPivot('section_id', 'term_id')
            ->withTimestamps();
    }

    /**
     * get all history of sections related to this teacher
     */
    public function sectionRecords()
    {
        return $this->belongsToMany('App\Section', 'teacher_records')
            ->withPivot('term_id')
            ->withTimestamps();
    }
}

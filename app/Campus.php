<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Campus extends Model
{
    use SoftDeletes, Uuids;
    protected $dates = ['deleted_at'];
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    /**
     * table name
     *
     * @var string
     */
    protected $table = "campuses";

    /**
     * attendanceMachines return array of model'AttendanceMachine'
     * objects
     *
     * @return Array \App\AttendanceMachine
     */
    public function attendanceMachines()
    {
        return $this->hasMany('App\AttendanceMachine');
    }

    /**
     * get all users related to this campus
     *
     * @return Array \App\User
     */
    public function users(){
        return $this->belongsToMany('App\User')
            ->withTimestamps();
    }

}

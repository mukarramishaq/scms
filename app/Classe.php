<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Classe extends Model
{
    use SoftDeletes, Uuids;
    protected $dates = ['deleted_at'];
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    /**
     * table name
     *
     * @var string
     */
    protected $table = "classes";

    /**
     * Retrieve related term
     */
    public function term()
    {
        return $this->belongsTo('App\Term');
    }

    /**
     * Retrieve all sections of a class
     */
    public function sections()
    {
        return $this->hasMany('App\Section');
    }
}

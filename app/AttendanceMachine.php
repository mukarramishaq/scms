<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AttendanceMachine extends Model
{
    use SoftDeletes, Uuids;
    protected $dates = ['deleted_at'];
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    
    /**
     * @inheritDoc
     *
     * @var string
     */
    protected $table = "attendance_machines";

    /**
     * This will return campus object related to this machine
     *
     * @return \App\Campus
     */
    public function campus()
    {
        return $this->belongsTo('App\Campus');
    }
}

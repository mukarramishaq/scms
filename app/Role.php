<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes, Uuids;
    protected $dates = ['deleted_at'];
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Retrieve all the users related to this role
     */
    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }
}

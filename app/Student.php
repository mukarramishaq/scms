<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes, Uuids;
    protected $dates = ['deleted_at'];
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    //
    /**
     * Retrieve related user
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Retrieve all the courses related to this student
     */
    public function subjects()
    {
        return $this->belongsToMany('App\Subject')
            ->withPivot('term_id')
            ->withTimestamps();
    }

    /**
     * Get all history of sections related to this student
     */
    public function sectionRecords()
    {
        return $this->belongsToMany('App\Section')
            ->withPivot('term_id')
            ->withTimestamps();
    }

    /**
     * get all guardians related to this student
     */
    public function guardians()
    {
        return $this->belongsToMany('App\Guardian')
            ->withPivot('is_primary_guardian')
            ->withTimestamps();
    }

}

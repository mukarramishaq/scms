<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Section extends Model
{
    use SoftDeletes, Uuids;
    protected $dates = ['deleted_at'];
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    //
    /**
     * Retrieve related class
     */
    public function classe()
    {
        return $this->belongsTo('App\Classe');
    }

    /**
     * get all records of students related to this section
     */
    public function studentRecords()
    {
        return $this->belongsToMany('App\Student', 'student_records')
            ->withPivot('term_id')
            ->withTimestamps();
    }

    /**
     * get all history of teachers related to this section
     */
    public function teacherRecords()
    {
        return $this->belongsToMany('App\Teacher', 'teacher_records')
            ->withPivot('term_id')
            ->withTimestamps();
    }
}

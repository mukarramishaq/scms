<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, SoftDeletes, Uuids;
    protected $dates = ['deleted_at'];
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Retrieve attendances
     */
    public function attendances()
    {
        return $this->hasMany('App\Attendance');
    }

    /**
     * retrieve related student
     */
    public function student()
    {
        return $this->hasOne('App\Student');
    }

    /**
     * retrieve related teacher
     */
    public function teacher()
    {
        return $this->hasOne('App\Teacher');
    }

    /**
     * retrieve related admin
     */
    public function admin()
    {
        return $this->hasOne('App\Admin');
    }

    /**
     * retrieve related guardian
     */
    public function guardian()
    {
        return $this->hasOne('App\Guardian');
    }

    /**
     * Retrieve all the roles related to this user
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role')->withTimestamps();
    }

    /**
     * Retreive all the campuses related to this user
     *
     * @return Array \App\Campus
     */
    public function campuses()
    {
        return $this->belongsToMany('App\Campus')
            ->withTimestamps();
    }
}

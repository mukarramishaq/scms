<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class DoesTeacherExist implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        return \DB::table('users')->where('users.'.$attribute, $value)
        ->join('role_user', 'role_user.user_id', 'users.id')
        ->join('roles', 'roles.id', 'role_user.role_id')
        ->where('roles.name', __('roles.teacher'))
        ->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ucfirst(__('roles.teacher')).' with this :attribute does not exist';
    }
}

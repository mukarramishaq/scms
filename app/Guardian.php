<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Guardian extends Model
{
    use SoftDeletes, Uuids;
    protected $dates = ['deleted_at'];
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * get all students related to this guardian
     */
    public function students()
    {
        return $this->belongsToMany('App\Student')
            ->withPivot('is_primary_guardian')
            ->withTimestamps();
    }
}

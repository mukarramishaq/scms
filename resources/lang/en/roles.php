<?php

return [
  'student' => 'student',
  'teacher' => 'teacher',
  'guardian' => 'guardian',
  'admin' => 'admin',
];
<?php

return [

  /*
  |--------------------------------------------------------------------------
  | HTTP Error Language Lines
  |--------------------------------------------------------------------------
  |
  | The following language lines are used during authentication for various
  | messages that we need to display to the user. You are free to modify
  | these language lines according to your application's requirements.
  |
  */
  'response_type'=>[
    'error'=>'error',
    'success'=>'succes',
  ],
  'success' => [
    'ok' => ['code' => '200', 'status' => 'OK'],
    'created' => ['code' => '201', 'status' => 'Created'],
    'accepted' => ['code' => '202', 'status' => 'Accepted'],
    'non_authoritative_information' => ['code' => '203', 'status' => 'Non-Authoritative Information'],
    'no_content' => ['code' => '204', 'status' => 'No Content'],
    'reset_content' => ['code' => '205', 'status' => 'Reset Content'],
  ],
  'client_error' => [
    'bad_request' => ['code' => '400', 'status' => 'Bad Request'],
    'unauthorized' => ['code' => '401', 'status' => 'Unauthorized'],
    'payment_required' => ['code' => '402', 'status' => 'Payment Required'],
    'forbidden' => ['code' => '403', 'status' => 'Forbidden'],
    'not_found' => ['code' => '404', 'status' => 'Not Found'],
    'method_not_allowed' => ['code' => '405', 'status' => 'Method Not Allowed'],
    'not_acceptable' => ['code' => '406', 'status' => 'Not Acceptable'],
  ],
  'server_error' => [
    'internal_server_error' => ['code' => '500', 'status' => 'Internal Server Error'],
    'not_implemented' => ['code' => '501', 'status' => 'Not Implemented'],
    'bad_gateway' => ['code' => '502', 'status' => 'Bad Gateway'],
  ],

];

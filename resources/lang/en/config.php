<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Configuration Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for main things like
    | institute name, license etc.
    |
    */

    'institute' => [
        'name' => 'The Educators',
        'branch' => 'Anabat Campus',
        'abbreviation' => 'AL',
    ],
    'company' => [
        'name' => 'Naix',
        'members' => [

        ],
        'contact' => [
            'website' => 'www.naix.com',
            'email' => 'contact@naix.com',
            'mobile_number' => '0900-78601',
            'phone_number' => '0900-78601',
        ]
    ],

];

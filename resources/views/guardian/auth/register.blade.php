@extends('layouts.app2')

@section('title')
Register
@endsection

@section('content')

<div class="row">
  <div class="card card-chart col-md-4 center">
    <div class="card-header card-header-primary">
      <h3 class="card-title">{{ __('Register') }}</h3>
      <p class="card-category">As {{ucfirst(__('roles.guardian'))}}</p>
    </div>
    <div class="card-body">
    @include('components.alert-errors')
    @include('components.alert-info')
    <form action="{{route('guardian.auth.register.post')}}" method="POST">
      @csrf
      <div class="form-group">
        <label for="name" class="label">Name</label>
        <input type="text" class="form-control" placeholder="Shakeel Ahmed" required="true" name="name" id="name" value="{{old('name')}}" >
      </div>
      <div class="form-group">
        <label for="email" class="label">Email Address</label>
        <input type="email" class="form-control" placeholder="shakeel.ahmed@anabateducators.com" required="true" name="email" id="email" value="{{old('email')}}" >
      </div>
      <div class="form-group">
        <label for="password" class="label">Password</label>
        <input type="password" class="form-control" required="true" name="password" id="password" value="{{old('password')}}" >
      </div>
      <div class="form-group">
        <label for="password_confirmation" class="label">Confirm Password</label>
        <input type="password" class="form-control" required="true" name="password_confirmation" id="password_confirmation" value="{{old('password_confirmation')}}" >
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-primary">{{ __('Register') }}</button>
      </div>
    </form>
    </div>
    <div class="card-footer">
      <div class="stats">
        The Educators, Anabat Campus
      </div>
    </div>
  </div>
</div>

@endsection
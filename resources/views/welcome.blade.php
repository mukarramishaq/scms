@extends('layouts.app2')

@section('title')
Welcome
@endsection

@section('content')

    <div class="row">
        <div class="card card-stats col-lg-6 col-md-6 col-sm-6">
            <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                <i class="material-icons">school</i>
                </div>
                <p class="card-category">{{__('config.institute.name')}}</p>
                <h3 class="card-title">{{__('config.institute.branch')}}</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                <i class="material-icons">update</i> We are coming soon!
                </div>
            </div>
        </div>
    </div>

@endsection
@extends('layouts.app2')

@section('content')
    <div class="row">
        <div class="card card-chart col-md-4 center">
            <div class="card-header card-header-primary">
            <h3 class="card-title">Login As</h3>
            </div>
            <div class="card-body">
                <br><a href="{{route('student.auth.login.get')}}" class="col-md-12 btn btn-primary">Student</a>
                <br><a href="{{route('teacher.auth.login.get')}}" class="col-md-12 btn btn-primary">Teacher</a>
                <br><a href="{{route('guardian.auth.login.get')}}" class=" col-md-12 btn btn-primary">Parents</a>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
@endsection

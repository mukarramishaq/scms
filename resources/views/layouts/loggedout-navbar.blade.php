<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-primary  fixed-top">
<div class="container-fluid">
    <div class="navbar-wrapper">
      <a class="navbar-brand" href="{{route('welcome')}}">{{__('config.institute.name')}}</a>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
    <span class="sr-only">Toggle navigation</span>
    <span class="navbar-toggler-icon icon-bar"></span>
    <span class="navbar-toggler-icon icon-bar"></span>
    <span class="navbar-toggler-icon icon-bar"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end">
    <ul class="navbar-nav">
        @include('layouts.loggedout-navitems')
    </ul>
    </div>
</div>
</nav>
<!-- End Navbar -->
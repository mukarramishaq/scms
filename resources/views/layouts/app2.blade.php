<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('theme/assets/img/apple-icon.png')}}">
  <link rel="icon" type="image/png" href="{{asset('theme/assets/img/favicon.png')}}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    @yield('title') | {{__('config.institute.name')}} | {{__('config.institute.branch')}}
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{asset('theme/assets/css/material-dashboard.css?v=2.1.0')}}" rel="stylesheet" />
  @yield('header-styles')
  @yield('header-scripts')
</head>

<body class="">
  <div class="wrapper ">
    @auth
    <!-- Sidebar -->
      @include('layouts.sidebar')
    @else
      @include('layouts.loggedout-sidebar')
    <!-- End Sidebar -->
    @endauth
    <div class="main-panel">
      @auth
      <!-- Navbar -->
        @include('layouts.navbar')
      @else
        @include('layouts.loggedout-navbar')
      <!-- End Navbar -->
      @endauth
      <div class="content">
        <div class="container-fluid">
          <!-- your content here -->
          @yield('content')
        </div>
      </div>
      @auth
      <!-- Footer -->
      @include('layouts.footer')
      @else
      @include('layouts.loggedout-footer')
      <!-- End Footer -->
      @endauth
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="{{asset('theme/assets/js/core/jquery.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('theme/assets/js/core/popper.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('theme/assets/js/core/bootstrap-material-design.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('theme/assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
  <!-- Chartist JS -->
  <script src="{{asset('theme/assets/js/plugins/chartist.min.js')}}"></script>
  <!--  Notifications Plugin    -->
  <script src="{{asset('theme/assets/js/plugins/bootstrap-notify.js')}}"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{asset('theme/assets/js/material-dashboard.min.js?v=2.1.0')}}" type="text/javascript"></script>
  @yield('footer-styles')
  @yield('footer-scripts')
</body>

</html>

<ul class="nav navbar-nav nav-mobile-menu">
  @auth
    @include('layouts.nav-items')
  @else
    @include('layouts.loggedout-navitems')
  @endauth
</ul>
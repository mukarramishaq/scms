<footer class="footer">
    <div class="container-fluid">
        <nav class="float-right">
        <ul>
            <li>
                <a href="{{route('welcome')}}">
                &copy;{{__('config.institute.name').', '. __('config.institute.branch')}}
                </a>
            </li>
        </ul>
        </nav>
        {{-- <div class="copyright float-right">
            Contact
            <a href="{{route('developer.contact')}}" target="_blank">SCMS</a> for better campus management systems.
        </div> --}}
        <!-- your footer here -->
    </div>
</footer>
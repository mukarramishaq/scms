<div class="sidebar" data-color="{{__('sidebar.color')}}" data-background-color="{{__('sidebar.background.color')}}" data-image="{{asset('theme/assets/image/sidebar-1.jpg')}}">
    <!--
    Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

    Tip 2: you can also add an image using data-image tag
    -->
    <div class="logo">
    <a href="http://www.creative-tim.com" class="simple-text logo-normal">
        @for($i = 0, $roles = Auth::user()->roles, $count = count($roles); $i < $count ; $i++)
            {{$roles[$i]->name}}
            @if ($i+1 < $count)
            |
            @endif
        @endfor
    </a>
    </div>
    <div class="sidebar-wrapper">
    @include('layouts.navbar-mobile')
    <ul class="nav">
        <li class="nav-item active  ">
        <a class="nav-link" href="#0">
            <i class="material-icons">dashboard</i>
            <p>Dashboard</p>
        </a>
        </li>
        <!-- your sidebar here -->
    </ul>
    </div>
</div>

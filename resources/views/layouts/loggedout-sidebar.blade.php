<div class="sidebar nav-mobile-menu" data-color="{{__('sidebar.color')}}" data-background-color="{{__('sidebar.background.color')}}" data-image="{{asset('theme/assets/image/sidebar-1.jpg')}}">

    <div class="sidebar-wrapper">
    @include('layouts.navbar-mobile')
    </div>
</div>
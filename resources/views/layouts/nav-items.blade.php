<li class="nav-item">
<a class="nav-link" href="#pablo">
    <i class="material-icons">notifications</i> Notifications
</a>
</li>
<li class="nav-item dropdown">
    <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        <i class="material-icons">person</i> {{Auth::user()->name}}
    <div class="ripple-container"></div></a>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
        {{-- <a class="dropdown-item" href="#">Mike John responded to your email</a>
        <a class="dropdown-item" href="#">You have 5 new tasks</a>
        <a class="dropdown-item" href="#">You're now friend with Andrew</a>
        <a class="dropdown-item" href="#">Another Notification</a> --}}
        <a class="dropdown-item" href="{{route('student.auth.logout')}}">Logout</a>
    </div>
</li>
<!-- your navbar here -->
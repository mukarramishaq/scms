@extends('layouts.app2')

@section('title')
Login
@endsection

@section('content')

<div class="row">
  <div class="card card-chart col-md-4 center">
    <div class="card-header card-header-primary">
      <h3 class="card-title">{{ __('Login') }}</h3>
      <p class="card-category">As {{ucfirst(__('roles.admin'))}}</p>
    </div>
    <div class="card-body">
    @include('components.alert-errors')
    @include('components.alert-info')
    <form action="{{route('admin.auth.login.post')}}" method="POST">
      @csrf
      <div class="form-group">
        <label for="email" class="label">Email Address</label>
        <input type="email" class="form-control" placeholder="shakeel.ahmed@anabateducators.com" required="true" name="email" id="email" value="{{old('email')}}" >
      </div>
      <div class="form-group">
        <label for="password" class="label">Password</label>
        <input type="password" class="form-control" required="true" name="password" id="password" value="{{old('password')}}" >
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-primary">{{ __('Login') }}</button>
      </div>
    </form>
    </div>
    <div class="card-footer">
      <div class="stats">
        The Educators, Anabat Campus
      </div>
    </div>
  </div>
</div>

@endsection
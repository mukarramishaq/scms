@if ($errors->any())
    <div class="alert alert-danger col-md-12">
        <p><b>ERROR:</b></p>
        <ul class="list">
            @foreach ($errors->all() as $error)
                <li class="list-item">{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
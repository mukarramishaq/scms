@if (session('info'))
    <div class="alert alert-{{session('info')['type'] == 'error' ? 'danger' : session('info')['type']}} col-md-12">
        <p><b>{{strtoupper(session('info')['type'])}}:</b><br>{!!session('info')['message']!!}</p>
    </div>
@endif